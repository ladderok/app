const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const config = {
    context: path.resolve(__dirname, 'assets'),
    entry: {
        app: './js/app.jsx',
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: './js/[chunkhash].js'
    },
    module: {
        rules: [
            {
                test: /\.js$|.jsx$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                },
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [{
                        loader: 'css-loader',
                        options: {
                            minimize: true,
                        }
                    }],
                }),
            },
        ],
    },
    plugins: [
        new CleanWebpackPlugin(['dist']),
        new ExtractTextPlugin({ filename: 'css/[chunkhash].[name].css' }),
        new HtmlWebpackPlugin({ template: '../index.html'}),
    ],
};

module.exports = config;
