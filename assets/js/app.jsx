import React from 'react';
import { render } from 'react-dom';
import '../css/index.css';

class App extends React.Component {
    constructor() {
        super();

        this.state = {
            name: '',
            secondName: '',
            q: '',
            users: [],
            error: '',
        };

        this.addUser = this.addUser.bind(this);
    }

    addUser(e) {
        e.preventDefault();

        const { name, secondName, users } = this.state;

        if (!name.trim().length || !secondName.trim().length) {
            this.setState({ error: 'Please enter user name and second name' });
            return;
        }

        const user = {
            id: users.length + 1,
            name,
            secondName,
        };

        const newUsers = [...users, user];

        this.setState({
            users: newUsers,
            name: '',
            secondName: '',
        }, () => {
            document.getElementById('name').focus();
        });
    }

    removeUser(id) {
        const { users } = this.state;

        const newUsers = users.filter(user => {
            return user.id !== id;
        });

        this.setState({ users: newUsers });
    }

    onChange(e, name) {
        this.setState({
            [name]: e.target.value,
            error: '',
        });
    }

    render() {
        const { name, secondName, error, users, q } = this.state;
        let usersToShow = [];

        if (q.length) {
            users.map(user => {
                const fullname = user.name + ' ' + user.secondName;
                if (fullname.search(q) !== -1) {
                    usersToShow.push(user);
                }
            });
        } else {
            usersToShow = users;
        }

        usersToShow.sort((a, b) => a.name.localeCompare(b.name));

        return (
            <div className="app">
                <form
                    className="form"
                    onSubmit={this.addUser}
                >
                    <input
                        type="text"
                        className="input"
                        id="name"
                        placeholder="User name"
                        onChange={(e) => { this.onChange(e, 'name'); }}
                        value={name}
                    />
                    <input
                        type="text"
                        className="input"
                        placeholder="User second name"
                        onChange={(e) => { this.onChange(e, 'secondName'); }}
                        value={secondName}
                    />
                    <button className="submit">
                        Add user
                    </button>
                </form>
                <input
                    className="input search"
                    type="text"
                    onChange={(e) => { this.onChange(e, 'q'); }}
                    placeholder="Search for user.."
                    value={q}
                />
                { error.length ?
                    <div className="error">{error}</div>
                : null }
                { usersToShow.length ?
                    <div className="users-list">
                        {usersToShow.map(user => (
                            <div className="user">
                                <p>{user.name}</p>
                                <p>{user.secondName}</p>
                                <button
                                    className="delete-btn"
                                    onClick={() => { this.removeUser(user.id); }}
                                >
                                    Delete user
                                </button>
                            </div>
                        ))}
                    </div>
                :
                    <div className="users-list--empty">
                        Users list is empty
                    </div>
                }
            </div>
        );
    }
};

render(<App />, document.getElementById('app'))
