## How to Run
```
clone repository
```
```
npm install
```
```
npm run dev
```
```
Open http://localhost:8080 in your browser
```
```
If in some reasons you want to see compiled files, run "npm run build".
Files will be located in dist folder
```

